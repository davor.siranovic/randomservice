# consumer service

This is a first service from RBA task that produces random numbers and deals 
with them as specified:

Prvi servis:

1.       Generira nasumične cijele brojeve po pozivu metode.


To analyze and run the consumer service in can be cloned from this address:

https://gitlab.com/davor.siranovic/randomservice/-/tree/main/random_producer

To start the service it is needed RandomNumberGenerator to run as Java app. 
The service is producing random number for the second service, so 
it is needed this service to be ran first.

This service runs on localhost:8081

It's output is:

http://localhost:8081/randomNumber

Main logic is in the RandomNumberContoller



