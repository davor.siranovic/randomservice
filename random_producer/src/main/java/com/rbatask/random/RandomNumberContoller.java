package com.rbatask.random;

import java.util.Random;

import org.springframework.boot.SpringApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rbatask.beans.RandomNumberBean;

@RestController
public class RandomNumberContoller {
	@GetMapping("/randomNumber")
	public RandomNumberBean getDemoBeanOut() {
		return new RandomNumberBean(generateRandomNumber());
	}
	
    public int generateRandomNumber() {
        Random random = new Random(); 
        return random.nextInt(); 
    }
}
